# HeroicHeroesOfHeronia - public


## What is it?

It's a cross-platform 2D game T've been developping as a hobby. 

If you want to see some game printscreen or learn more about the gameplay, go have a look at the [game website](https://www.heroes-heronia.com/)


## Techs implied

- Native Java using the [LibGDX framework](https://libgdx.com/)
- Developped on Android Studio
- 2D visual assets creation using AsespriteMultiple programming patterns implied (Abstract Factory, Object pooling, Prototype, Singleton, Observer, Dependecy Injection, State pattern, Chain of Responsibility, MVC-like architecture, Builder)   

--> Might seems like a random list of patterns, but all of these have consciously been implemented... Come have chat to learn more and discuss!


## Gameplay core features

- Nervous and simple : The gameplay tends to be straightforward and uncomplicated, involving minimal mechanics or - rules
- In-depth AI : The mobs actions and movements are dictated by a deep AI system made to be variable and unpredictable
- Dynamic character switch : Your team consists of three heroes, which you can quickly switch between, letting the AI manage the other two
- Rewarding gameplay : Each enemy killed, each room cleaned : everything is made to be rewarding and help you level up your game


## Download the latest beta

[Find the beta](https://www.heroes-heronia.com/beta/)

